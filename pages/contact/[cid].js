import React, { useState, useEffect } from 'react';
import { fetchContacts } from '../../services/contacts';

function Contact({ params }) {
    //console.log('Contact', params);

    const [contact, setContact] = useState(undefined);

    useEffect(() => {
        if(params){
            fetchContacts().then(data => {
                //他會回傳一個array!
                const fouscContact = data.filter(arr => arr.id === params.cid)[0];
                setContact(fouscContact);
            });
        }
    }, [params]);

    const renderContact = contact ? (
        <div>
            {contact.id} : {contact.name}
        </div>
    ) : (null);

    return (
        <div>
            This is page Contact!
            {renderContact}
        </div>
    );
}

export async function getStaticPaths() {
    return { 
        paths: [
            { params: {cid: '011XZU'} }
        ],
        fallback: true 
    };
}

export async function getStaticProps({ params }) {
    //console.log('getStaticProps', params);

    return {
      props: {
        params,
      },
    };
}

export default Contact;