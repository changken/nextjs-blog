import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Card from '../components/Card';

export default function Home({ articles }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        {articles ? articles.map(art => <Card article={art} />) : null}
      </main>
    </div>
  )
}
