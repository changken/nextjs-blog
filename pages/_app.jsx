import React, { useEffect } from 'react';
import { createWrapper } from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import { compose, createStore } from 'redux';
import '../assets/style/index.scss';
import BasicLayout from '../layouts/BasicLayout';
import AdminLayout from '../layouts/AdminLayout';
import { fetchArticles } from '../services/article';
import rootReducer from '../store';

//google擴充套件!
const composeEnhancers = 
  (typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || 
    compose;

const store = createStore(rootReducer, {articles: null}, composeEnhancers);

function MyApp({Component, pageProps, router }) {
  if(router.pathname.split('/')[1] === 'admin'){
    return (
      <AdminLayout>
        <Component {...pageProps}/>
      </AdminLayout>
    );
  }

  return (
    <Provider store={store}>
      <BasicLayout>
        <Component {...pageProps}/>
      </BasicLayout>
    </Provider>
  );
}

const makeStore = () => store;
const wrapper = createWrapper(makeStore);

export default wrapper.withRedux(MyApp);