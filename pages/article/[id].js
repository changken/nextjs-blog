import React, {useEffect, useState} from 'react';
import { fetchArticles } from '../../services/article';

function Article({ params }) {
    const [article, setArticle] = useState(undefined);

    useEffect(() => {
        if(params){
            fetchArticles().then(data => {
                const foucsArticle = data.filter(arr => arr.id === params.id)[0];
                setArticle(foucsArticle);
            });
        }
      }, [params]);

    return article ? (
        <div>
            This is article! {article.title}
        </div>
    ) : null;
}

export async function getStaticPaths() {
    return { 
        paths: [
            { 
                params: {
                    id: '-M-di4iSxbuAxMPTsXP_'
                } 
            }
        ], 
        fallback: true 
    };
}

export async function getStaticProps({ params }) {
    return {
      props: {
        params,
      },
    };
}

export default Article;