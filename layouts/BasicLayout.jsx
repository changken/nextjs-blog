import React, {useEffect, useState, Children, cloneElement} from 'react';
import Nav from '../components/Nav';
import { fetchArticles } from '../services/article';

function BasicLayout({ children }) {
    const [articles, setArticles] = useState();

    useEffect(() => {
        fetchArticles().then(data => {
            setArticles([...data]);
        });
    }, [])

    return (
        <div className="layout-basic">
            <Nav />
            {Children.map(children, (child) => {
                return cloneElement(child, {
                    articles: articles
                });
            })}
        </div>
    );
}

export default BasicLayout;