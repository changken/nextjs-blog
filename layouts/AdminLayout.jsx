import React, {useState, useEffect, Children, cloneElement} from 'react';
import { fetchArticles } from '../services/article';
import Link from 'next/link';

function AdminLayout({ children }) {
    const [articles, setArticles] = useState();

    useEffect(() => {
        fetchArticles().then(data => {
            setArticles([...data]);
        });
    }, []);

    return (
        <div className="layout-admin">
            <section className="side-nav">
                <li><Link href="/admin/"><a>articles</a></Link></li>
                <li><Link href="/admin/add"><a>add</a></Link></li>
                <li><Link href="/admin/category"><a>category</a></Link></li>

            </section>
            {Children.map(children, (child) => {
                    return cloneElement(child, {
                        articles: articles
                    });
                })}
        </div>
    );
}

export default AdminLayout;