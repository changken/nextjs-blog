import { combineReducers } from 'redux';
import {articleReducer} from './models/article';

const rootReducer = combineReducers({
    articles: articleReducer,
});

export default rootReducer;