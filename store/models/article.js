import _ from 'lodash';

export const fetchArticle = data => {
    return {
        type: "FETCH",
        payload: data
    }
};

export const addArticle = data => {
    const id = "ART" + ("0000" + _.random(0, 10000)).slice(-4);
    
    return {
        type: "FETCH",
        payload: {id: id, ...data}
    }
};

export const updateArticle = params => {
    return {
        type: "UPDATE",
        payload: params
    }
};

export const deleteArticle = id => {
    return {
        type: "DELETE",
        payload: id
    }
};

export const articleReducer = (state = { articles: null}, action) => {
    switch(action.type){
        case "UPDATE":
            const idx = state.articles
            .map(art => art.id)
            .indexOf(action.payload.id);
            const updateArticles = [...state.articles];
            updateArticles.splice(idx,1,action.payload);
            return {...state, articles: [...updateArticles]};
        
            case "DELETE":
            const newArticles = state.articles.filter(
                art => art.id !== action.payload
            );
            return {...state, articles: [...newArticles]};
        
            case "ADD":
                return {...state, articles:[action.payload, ...state.articles]};
            
            case "FETCH":
                return {...state, articles: action.payload};
            
            default:
                return {...state};
    }
};