import React from 'react';

function Card({ article }) {
    return (
        <div className="component-card">
            <img src={article.banner} alt="" />
            <h3>{article.title}</h3>
        </div>
    );
}

export default Card;