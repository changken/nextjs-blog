import React from 'react';
import Link from 'next/link';

function Nav(props) {
    return (
        <div className="component-nav">
            <div className="brand">
                glove coding
            </div>
            <ul>
                <li>
                    <Link href="/"><a>Home</a></Link>
                </li>
                <li>
                    <Link href="/article"><a>article</a></Link>
                </li>
                <li>
                    <Link href="/about"><a>about</a></Link>
                </li>
                <li>
                    <Link href="/category"><a>category</a></Link>
                </li>
                <li>
                    <Link href="/contact"><a>contact</a></Link>
                </li>
            </ul>
        </div>
    );
}

export default Nav;