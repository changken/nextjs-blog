export async function fetchArticles() { 
    const response = await fetch("https://us-central1-expressapi-8c039.cloudfunctions.net/app/article");
    const data = await response.json();

    return data.data;
}