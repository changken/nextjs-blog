export const fetchContacts = async () => {
    const response = await fetch('https://cors-anywhere.herokuapp.com/https://data.ntpc.gov.tw/api/datasets/365714DB-5840-46E6-AD3F-CD5BF6460D20/json?page=0&size=100');
    const data = await response.json();

    return data;
};